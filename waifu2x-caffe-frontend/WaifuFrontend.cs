using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Forms;
using System.Diagnostics;

//TODO: Clean up redundant code

namespace waifu2x_caffe_frontend
{
    public partial class WaifuFrontend : Form
    {
        private const string PROGRAM_ARGS = "{0} {1} -i \"{2}\" -o \"{3}\"";
        private const string PROGRAM_PATH = "waifu2x-caffe-cui.exe";

        private SaveFileDialog m_SaveDialog = new SaveFileDialog();
        private OpenFileDialog m_OpenDialog = new OpenFileDialog();
        private FolderBrowserDialog m_BrowserDialog = new FolderBrowserDialog();
        private BackgroundWorker m_BackgroundWorker = new BackgroundWorker();

        #region File formats
        private const string PNG = "PNG Portable Network Graphics (*.png)|" + "*.png";
        private const string JPG = "JPEG File Interchange Format (*.jpg, *.jpeg, *jfif)|" + "*.jpg;*.jpeg;*.jfif";
        private const string BMP = "BMP Windows Bitmap (*.bmp)|" + "*.bmp";
        private const string TIF = "TIF Tagged Imaged File Format (*.tif, *.tiff)|" + "*.tif;*.tiff";
        private const string ALL_IMAGES = "Image file|" + "*.png; *.jpg; *.jpeg; *.jfif; *.bmp;*.tif; *.tiff;";
        private const string ALL_FILES = "All files (*.*)|" + "*.*";

        //private List<string> m_ValidFiles = new List<string>();
        private static readonly string[] VALID_EXTENSIONS = {".png", ".jpg", ".jpeg", "*.jfif",".bmp", ".tif", ".tiff"};

        #endregion

        public WaifuFrontend()
        {
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            this.AutoSize = true;
            this.AutoSizeMode = AutoSizeMode.GrowAndShrink;

            InitializeComponent();

            // Setup OpenDialog
            m_OpenDialog.Filter = PNG + "|" + JPG + "|" + BMP + "|" + TIF + 
                                        "|" + ALL_IMAGES + "|" + ALL_FILES;
            m_OpenDialog.RestoreDirectory = true;

            // Setup SaveDialog
            m_SaveDialog.Filter = "PNG (*.png)|*.png|All files (*.*)|*.*";
            m_SaveDialog.RestoreDirectory = true;

            //m_BackgroundWorker.DoWork += Form1_DoWork;
            m_BackgroundWorker.RunWorkerCompleted += Form1_RunWorkerCompleted;
            m_BackgroundWorker.WorkerSupportsCancellation = true;
        }

        #region Form Set/Get
        private void SetNoiseReduction(int mode)
        {
            switch (mode) {
                case 0:
                    ReductionNoneButton.Checked = true;
                    break;
                case 1:
                    ReductionOneButton.Checked = true;
                    break;
                case 2:
                    ReductionTwoButton.Checked = true;
                    break;
                case 3:
                    ReductionAutoButton.Checked = true;
                    break;
                default:
                    Console.WriteLine("Invalid mode setting");
                    break;
            }
        }

        private int GetNoiseReduction()
        {
            if (ReductionNoneButton.Checked) {
                return 0;
            } else if (ReductionOneButton.Checked) {
                return 1;
            } else if (ReductionTwoButton.Checked) {
                return 2;
            } else {
                return 3;
            }
        }

        private string GetModeString()
        {
            var noise = GetNoiseReduction();
            var mode = "";

            if (noise == 3) {
                // Automatic, whatever that means
                mode = "-m auto_scale -s " + ScaleUpDown.Value + " -n 1";
            } else if (ScaleUpDown.Value == 1 && noise > 1) {
                // Noise reduction without scale
                mode = "-m noise -n " + noise;
            } else if (ScaleUpDown.Value > 1 && noise > 1) {
                // Noise reduction and scale
                mode = "-m noise_scale -s " + ScaleUpDown.Value + " -n " + noise;
            } else {
                // Scale without noise reduction
                mode = "-m scale -s " + ScaleUpDown.Value;
            }
            return mode;
        }

        private void SetProcessor(int processor)
        {
            switch (processor) {
                case 0:
                    ProcessCpuButton.Checked = true;
                    break;
                case 1:
                    ProcessGpuButton.Checked = true;
                    break;
                case 2:
                    ProcessCudnnButton.Checked = true;
                    break;
                default:
                    Console.WriteLine("Invalid processor setting");
                    break;
            }
        }

        private int GetProcessor()
        {
            if (ProcessCpuButton.Checked) {
                return 0;
            } else if (ProcessGpuButton.Checked) {
                return 1;
            } else {
                return 2;
            }
        }

        private string GetProcessorString()
        {
            switch (GetProcessor()) {
                case 0:
                    return "-p cpu";
                case 1:
                    return "-p gpu";
                default:
                    return "-p cudnn";
            }
        }
        #endregion

        #region File Dialogs
        /// <summary>
        /// Opens a load file dialog
        /// </summary>
        /// <returns>Path to file</returns>
        private string LoadFileDialog()
        {
            if (m_OpenDialog.ShowDialog() == DialogResult.OK) {
                try {
                    if (m_OpenDialog.CheckFileExists) {
                        return m_OpenDialog.FileName;
                    }
                } catch (Exception ex) {
                    MessageBox.Show(ex.Message);
                }
            }
            return null;
        }
        /// <summary>
        /// Opens a save file dialog
        /// </summary>
        /// <returns>Path to save file</returns>
        private string SaveFileDialog()
        {
            if (m_SaveDialog.ShowDialog() == DialogResult.OK) {
                return m_SaveDialog.FileName;
            }
            return null;
        }
        /// <summary>
        /// Opens a folder browser dialog
        /// </summary>
        /// <returns>Path to folder</returns>
        private string SaveBrowserDialog()
        {
            this.Activate();
            if (m_BrowserDialog.ShowDialog() == DialogResult.OK) {
                return m_BrowserDialog.SelectedPath;
            }
            return null;
        }
        #endregion

        /// <summary>
        ///  Formats the settings and files into a string for use with waifu2x
        /// </summary>
        /// <param name="inFile">Path to file being converted</param>
        /// <param name="outFile">Path to output file</param>
        /// <returns>Arguments for waifu2x-caffe derived from settings</returns>
        private string GetArgs(string inFile, string outFile)
        {
            return string.Format(PROGRAM_ARGS, GetModeString(), GetProcessorString(), inFile, outFile);
        }

        // Add text to OutputTextBox
        private void WriteTextBox(string message)
        {
            this.Invoke(new MethodInvoker(delegate
            {
                OutputTextBox.AppendText(DateTime.Now.ToString("[HH:mm:ss] ") + message + "\n");
            }));
        }

        /// <summary>
        /// Setup m_BackgroundWorker and runs worker
        /// </summary>
        /// <param name="handler">Event handler for BackgroundWorker.DoWork</param>
        /// <param name="argsList">List of arguments for waifu2x-caffe</param>
        private void RunAsyncTask(DoWorkEventHandler handler, string[] argsList)
        {
            if (!m_BackgroundWorker.IsBusy) {
                Form1_DoWorkSetup();
                m_BackgroundWorker.DoWork += handler;

                // Check to see if args are being passed
                if (argsList == null) {
                    WriteTextBox("ERROR: Missing argument list.");
                } else {
                    m_BackgroundWorker.RunWorkerAsync(argsList);
                }
            } else {
                WriteTextBox("ERROR: New conversion task started while a task is running");
            }
        }

        /// <summary>
        /// Setup m_BackgroundWorker and runs worker
        /// </summary>
        /// <param name="handler">Event handler for BackgroundWorker.DoWork</param>
        /// <param name="args">Arguments for waifu2x-caffe</param> 
        private void RunASyncTask(DoWorkEventHandler handler, string args)
        {
            if (!m_BackgroundWorker.IsBusy) {
                Form1_DoWorkSetup();
                m_BackgroundWorker.DoWork += handler;

                // Check to see if args are being passed
                if (args.Length == 0) {
                    WriteTextBox("ERROR: Missing arguments");
                } else {
                    m_BackgroundWorker.RunWorkerAsync(args);
                }
            } else {
                WriteTextBox("ERROR: New conversion task started while a task is running");
            }
        }

        /// <summary>
        ///  Run waifu2x-caffe with arguments in a background process
        /// </summary>
        /// <param name="args">Arguments for waifu2x-caffe</param>
        private void RunWaifu2x(string args)
        {
            if (File.Exists(PROGRAM_PATH)) {
                var process = new Process();

                // Setup process vars
                process.StartInfo.UseShellExecute = false;
                process.StartInfo.RedirectStandardOutput = true;
                process.StartInfo.RedirectStandardError = true;
                process.StartInfo.CreateNoWindow = true;
                process.StartInfo.FileName = PROGRAM_PATH;
                process.StartInfo.Arguments = args;
                process.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;

                WriteTextBox(PROGRAM_PATH + " " + args);

                // Start process and wait for either a cancelation event or for the process to finish
                process.Start();
                while (!process.HasExited) {
                    if (m_BackgroundWorker.CancellationPending) {
                        process.Kill();
                        WriteTextBox("Conversion aborted.");
                        continue;
                    } else {
                        System.Threading.Thread.Sleep(500);
                    }
                }
                // Add output to textbox
                var output = process.StandardOutput.ReadToEnd();
                if (output.Length != 0) {
                    WriteTextBox(PROGRAM_PATH + ": " + output);
                }
            } else {
                WriteTextBox("ERROR: Missing " + PROGRAM_PATH);
            }
        }

        /// <summary>
        /// Handler for run button; Convert a single file through a load dialog instead of drag 'n drop
        /// </summary>
        private void RunButton_Click(object sender, EventArgs e)
        {
            string filename = LoadFileDialog();
            string savefile = null;

            // Don't bring up save file dialog if user canceled out LoadFileDialog()
            if (filename != null) {
                savefile = SaveFileDialog();
            }
            
            // Start background waifu2x process
            if (filename != null && savefile != null) {
                string args = GetArgs(filename, savefile);
                RunASyncTask(Form1_DoWork, args);
            }
        }

        /// <summary>
        /// Handler for run button while waifu2x is running
        /// </summary>
        private void RunButton_Cancel(object sender, EventArgs e)
        {
            if (m_BackgroundWorker.IsBusy) {
                m_BackgroundWorker.CancelAsync();
            }
        }

        /// <summary>
        /// Handle drag and drop events
        /// </summary>
        private void Form1_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop)) {
                e.Effect = DragDropEffects.Copy;
            } else {
                e.Effect = DragDropEffects.None;
            }
        }

        /// <summary>
        ///  Convert files the fun way with drag and drop
        /// </summary>
        private void Form1_DragDrop(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop)) {
                string[] filePaths = (string[])(e.Data.GetData(DataFormats.FileDrop));

                // Multiple files dropped
                if (filePaths.Length > 1) {
                    var folderpath = SaveBrowserDialog();
                    var formattedArgs = new List<string>();
                    // Create waifu2x args from each file string
                    if (folderpath != null) {
                        foreach (var file in filePaths) {
                            if (File.Exists(file)) {
                                var f = Path.GetFileNameWithoutExtension(file);
                                formattedArgs.Add(GetArgs(file, folderpath + "\\" + f + "-waifu2x.png"));
                            } else {
                                WriteTextBox("ERROR (Multi-file): File does not exist: " + file +
                                                " File.Exists() returned " + File.Exists(file));
                            }
                        }

                        // Start background waifu proces
                        RunAsyncTask(Form1_DoWorkList, formattedArgs.ToArray<string>());
                    }
                } else {
                    var savefile = SaveFileDialog();
                    if (savefile != null && File.Exists(filePaths[0])) {
                        RunASyncTask(Form1_DoWork, GetArgs(filePaths[0], savefile));
                    } else if (File.Exists(filePaths[0])) {
                        WriteTextBox("ERROR (Single-file): File does not exist: " + filePaths[0] + 
                                        " File.Exists() returned " + File.Exists(filePaths[0]));
                    }
                }
            }
        }

        /// <summary>
        /// Handler for when waifu2x is already running
        /// </summary>
        private void Form1_DragDropWorking(object sender, DragEventArgs e)
        {
            MessageBox.Show("Hakase is currently busy with a drawing. Please wait until she's done.",
                            "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }

        /// <summary>
        /// Suppress keypresses so it doesn't ding when the user hits enter
        /// Also removes focus from the text box
        /// </summary>
        private void ScaleUpDown_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Escape) {
                this.ActiveControl = null;
                e.SuppressKeyPress = true;
            }
        }

        /// <summary>
        ///  Highlight all the text when the user enters
        /// </summary>
        private void ScaleUpDown_Enter(object sender, EventArgs e)
        {
            ScaleUpDown.Select(0, ScaleUpDown.Text.Length);
        }

        /// <summary>
        /// Hide/show details panel when clicked
        /// </summary>
        private void DetailsButton_Click(object sender, EventArgs e)
        {
            DetailsPanel.Visible = !DetailsPanel.Visible;
        }

        /// <summary>
        /// Handler for program loading. Restores previous settings
        /// </summary>
        private void WaifuFrontend_Load(object sender, EventArgs e)
        {
            ScaleUpDown.Value = Properties.Settings.Default.ScaleValue;
            SetProcessor(Properties.Settings.Default.Processor);
            SetNoiseReduction(Properties.Settings.Default.ReductionLevel);
        }

        /// <summary>
        /// Handler for program closing. Saves user settings
        /// </summary>
        private void Form1_Closing(object sender, FormClosingEventArgs e)
        {
            Properties.Settings.Default.ScaleValue = ScaleUpDown.Value;
            Properties.Settings.Default.Processor = GetProcessor();
            Properties.Settings.Default.ReductionLevel = GetNoiseReduction();
            Properties.Settings.Default.Save();
        }

        /// <summary>
        /// Assign different event handlers to events
        /// Used for when a task is running
        /// </summary>
        private void Form1_DoWorkSetup()
        {
            this.DragDrop -= Form1_DragDrop;
            this.DragDrop += Form1_DragDropWorking;
            RunButton.Text = "Cancel";
            RunButton.Click -= this.RunButton_Click;
            RunButton.Click += this.RunButton_Cancel;
            MainPictureBox.Image = Properties.Resources.hakase_drawing;
        }

        /// <summary>
        /// Run waifu2x
        /// </summary>
        /// <param name="e">Arguments for waifu2x-caffe</param>
        private void Form1_DoWork(object sender, DoWorkEventArgs e)
        {
            string args = e.Argument as string;
            RunWaifu2x(args);
        }

        /// <summary>
        /// Run waifu2x with a list of files to convert
        /// </summary>
        /// <param name="e">List of arguments for waifu2x-caffe</param>
        private void Form1_DoWorkList(object sender, DoWorkEventArgs e)
        {
            string[] args = e.Argument as string[];
            for (var i = 0; i < args.Length && !m_BackgroundWorker.CancellationPending; i++) {
                RunWaifu2x(args[i]);
            }
        }

        /// <summary>
        /// Assign default event handlers to events
        /// Used for when worker finishes its task
        /// </summary>
        private void Form1_RunWorkerCompleted(object sender, EventArgs e)
        {
            this.Activate();
            this.DragDrop -= Form1_DragDropWorking;
            this.DragDrop += Form1_DragDrop;
            m_BackgroundWorker.DoWork -= Form1_DoWork;
            m_BackgroundWorker.DoWork -= Form1_DoWorkList;

            RunButton.Text = "Load";
            RunButton.Click -= this.RunButton_Cancel;
            RunButton.Click += this.RunButton_Click;
            MainPictureBox.Image = Properties.Resources.hakase_drawing_static;
            WriteTextBox("Done!");

        }
    }
}
