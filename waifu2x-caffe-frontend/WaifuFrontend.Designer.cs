namespace waifu2x_caffe_frontend
{
    partial class WaifuFrontend
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.DetailsPanel = new System.Windows.Forms.Panel();
            this.OutputTextBox = new System.Windows.Forms.TextBox();
            this.ProcessorGroupBox = new System.Windows.Forms.GroupBox();
            this.ProcessCudnnButton = new System.Windows.Forms.RadioButton();
            this.ProcessCpuButton = new System.Windows.Forms.RadioButton();
            this.ProcessGpuButton = new System.Windows.Forms.RadioButton();
            this.ModeGroupBox = new System.Windows.Forms.GroupBox();
            this.ReductionAutoButton = new System.Windows.Forms.RadioButton();
            this.ReductionNoneButton = new System.Windows.Forms.RadioButton();
            this.ReductionTwoButton = new System.Windows.Forms.RadioButton();
            this.ReductionOneButton = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.ScaleUpDown = new System.Windows.Forms.NumericUpDown();
            this.DetailsButton = new System.Windows.Forms.Button();
            this.MainPictureBox = new System.Windows.Forms.PictureBox();
            this.RunButton = new System.Windows.Forms.Button();
            this.DetailsPanel.SuspendLayout();
            this.ProcessorGroupBox.SuspendLayout();
            this.ModeGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ScaleUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MainPictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // DetailsPanel
            // 
            this.DetailsPanel.Controls.Add(this.OutputTextBox);
            this.DetailsPanel.Controls.Add(this.ProcessorGroupBox);
            this.DetailsPanel.Controls.Add(this.ModeGroupBox);
            this.DetailsPanel.Controls.Add(this.label1);
            this.DetailsPanel.Controls.Add(this.ScaleUpDown);
            this.DetailsPanel.Location = new System.Drawing.Point(1, 260);
            this.DetailsPanel.Margin = new System.Windows.Forms.Padding(0);
            this.DetailsPanel.Name = "DetailsPanel";
            this.DetailsPanel.Size = new System.Drawing.Size(354, 298);
            this.DetailsPanel.TabIndex = 1;
            this.DetailsPanel.Visible = false;
            // 
            // OutputTextBox
            // 
            this.OutputTextBox.Location = new System.Drawing.Point(4, 129);
            this.OutputTextBox.Margin = new System.Windows.Forms.Padding(0);
            this.OutputTextBox.Multiline = true;
            this.OutputTextBox.Name = "OutputTextBox";
            this.OutputTextBox.ReadOnly = true;
            this.OutputTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.OutputTextBox.Size = new System.Drawing.Size(345, 163);
            this.OutputTextBox.TabIndex = 7;
            // 
            // ProcessorGroupBox
            // 
            this.ProcessorGroupBox.Controls.Add(this.ProcessCudnnButton);
            this.ProcessorGroupBox.Controls.Add(this.ProcessCpuButton);
            this.ProcessorGroupBox.Controls.Add(this.ProcessGpuButton);
            this.ProcessorGroupBox.Location = new System.Drawing.Point(179, 6);
            this.ProcessorGroupBox.Name = "ProcessorGroupBox";
            this.ProcessorGroupBox.Size = new System.Drawing.Size(170, 91);
            this.ProcessorGroupBox.TabIndex = 6;
            this.ProcessorGroupBox.TabStop = false;
            this.ProcessorGroupBox.Text = "Processor Mode";
            // 
            // ProcessCudnnButton
            // 
            this.ProcessCudnnButton.AutoSize = true;
            this.ProcessCudnnButton.Location = new System.Drawing.Point(11, 65);
            this.ProcessCudnnButton.Name = "ProcessCudnnButton";
            this.ProcessCudnnButton.Size = new System.Drawing.Size(61, 17);
            this.ProcessCudnnButton.TabIndex = 2;
            this.ProcessCudnnButton.Text = "cuDNN";
            this.ProcessCudnnButton.UseVisualStyleBackColor = true;
            // 
            // ProcessCpuButton
            // 
            this.ProcessCpuButton.AutoSize = true;
            this.ProcessCpuButton.Location = new System.Drawing.Point(11, 19);
            this.ProcessCpuButton.Name = "ProcessCpuButton";
            this.ProcessCpuButton.Size = new System.Drawing.Size(47, 17);
            this.ProcessCpuButton.TabIndex = 0;
            this.ProcessCpuButton.Text = "CPU";
            this.ProcessCpuButton.UseVisualStyleBackColor = true;
            // 
            // ProcessGpuButton
            // 
            this.ProcessGpuButton.AutoSize = true;
            this.ProcessGpuButton.Checked = true;
            this.ProcessGpuButton.Location = new System.Drawing.Point(11, 42);
            this.ProcessGpuButton.Name = "ProcessGpuButton";
            this.ProcessGpuButton.Size = new System.Drawing.Size(55, 17);
            this.ProcessGpuButton.TabIndex = 1;
            this.ProcessGpuButton.TabStop = true;
            this.ProcessGpuButton.Text = "CUDA";
            this.ProcessGpuButton.UseVisualStyleBackColor = true;
            // 
            // ModeGroupBox
            // 
            this.ModeGroupBox.Controls.Add(this.ReductionAutoButton);
            this.ModeGroupBox.Controls.Add(this.ReductionNoneButton);
            this.ModeGroupBox.Controls.Add(this.ReductionTwoButton);
            this.ModeGroupBox.Controls.Add(this.ReductionOneButton);
            this.ModeGroupBox.Location = new System.Drawing.Point(4, 6);
            this.ModeGroupBox.Name = "ModeGroupBox";
            this.ModeGroupBox.Size = new System.Drawing.Size(170, 117);
            this.ModeGroupBox.TabIndex = 5;
            this.ModeGroupBox.TabStop = false;
            this.ModeGroupBox.Text = "Noise Reduction";
            // 
            // ReductionAutoButton
            // 
            this.ReductionAutoButton.AutoSize = true;
            this.ReductionAutoButton.Location = new System.Drawing.Point(11, 88);
            this.ReductionAutoButton.Name = "ReductionAutoButton";
            this.ReductionAutoButton.Size = new System.Drawing.Size(47, 17);
            this.ReductionAutoButton.TabIndex = 3;
            this.ReductionAutoButton.Text = "Auto";
            this.ReductionAutoButton.UseVisualStyleBackColor = true;
            // 
            // ReductionNoneButton
            // 
            this.ReductionNoneButton.AutoSize = true;
            this.ReductionNoneButton.Checked = true;
            this.ReductionNoneButton.Location = new System.Drawing.Point(11, 19);
            this.ReductionNoneButton.Name = "ReductionNoneButton";
            this.ReductionNoneButton.Size = new System.Drawing.Size(51, 17);
            this.ReductionNoneButton.TabIndex = 0;
            this.ReductionNoneButton.TabStop = true;
            this.ReductionNoneButton.Text = "None";
            this.ReductionNoneButton.UseVisualStyleBackColor = true;
            // 
            // ReductionTwoButton
            // 
            this.ReductionTwoButton.AutoSize = true;
            this.ReductionTwoButton.Location = new System.Drawing.Point(11, 65);
            this.ReductionTwoButton.Name = "ReductionTwoButton";
            this.ReductionTwoButton.Size = new System.Drawing.Size(47, 17);
            this.ReductionTwoButton.TabIndex = 2;
            this.ReductionTwoButton.Text = "High";
            this.ReductionTwoButton.UseVisualStyleBackColor = true;
            // 
            // ReductionOneButton
            // 
            this.ReductionOneButton.AutoSize = true;
            this.ReductionOneButton.Location = new System.Drawing.Point(11, 42);
            this.ReductionOneButton.Name = "ReductionOneButton";
            this.ReductionOneButton.Size = new System.Drawing.Size(45, 17);
            this.ReductionOneButton.TabIndex = 1;
            this.ReductionOneButton.Text = "Low";
            this.ReductionOneButton.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(215, 106);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(34, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Scale";
            // 
            // ScaleUpDown
            // 
            this.ScaleUpDown.DecimalPlaces = 1;
            this.ScaleUpDown.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.ScaleUpDown.Location = new System.Drawing.Point(256, 103);
            this.ScaleUpDown.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.ScaleUpDown.Name = "ScaleUpDown";
            this.ScaleUpDown.Size = new System.Drawing.Size(47, 20);
            this.ScaleUpDown.TabIndex = 0;
            this.ScaleUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.ScaleUpDown.Click += new System.EventHandler(this.ScaleUpDown_Enter);
            this.ScaleUpDown.Enter += new System.EventHandler(this.ScaleUpDown_Enter);
            this.ScaleUpDown.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ScaleUpDown_KeyDown);
            // 
            // DetailsButton
            // 
            this.DetailsButton.Location = new System.Drawing.Point(12, 226);
            this.DetailsButton.Name = "DetailsButton";
            this.DetailsButton.Size = new System.Drawing.Size(81, 23);
            this.DetailsButton.TabIndex = 0;
            this.DetailsButton.Text = "Details";
            this.DetailsButton.UseVisualStyleBackColor = true;
            this.DetailsButton.Click += new System.EventHandler(this.DetailsButton_Click);
            // 
            // MainPictureBox
            // 
            this.MainPictureBox.Image = global::waifu2x_caffe_frontend.Properties.Resources.hakase_drawing_static;
            this.MainPictureBox.ImageLocation = "";
            this.MainPictureBox.Location = new System.Drawing.Point(-110, -10);
            this.MainPictureBox.Margin = new System.Windows.Forms.Padding(0);
            this.MainPictureBox.Name = "MainPictureBox";
            this.MainPictureBox.Size = new System.Drawing.Size(465, 270);
            this.MainPictureBox.TabIndex = 1;
            this.MainPictureBox.TabStop = false;
            // 
            // RunButton
            // 
            this.RunButton.Location = new System.Drawing.Point(12, 197);
            this.RunButton.Name = "RunButton";
            this.RunButton.Size = new System.Drawing.Size(81, 23);
            this.RunButton.TabIndex = 2;
            this.RunButton.Text = "Load";
            this.RunButton.UseVisualStyleBackColor = true;
            this.RunButton.Click += new System.EventHandler(this.RunButton_Click);
            // 
            // Form1
            // 
            this.AllowDrop = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(356, 555);
            this.Controls.Add(this.RunButton);
            this.Controls.Add(this.DetailsButton);
            this.Controls.Add(this.MainPictureBox);
            this.Controls.Add(this.DetailsPanel);
            this.Name = "Form1";
            this.Text = "Waifu2x-Caffe Frontend (Beta .98)";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_Closing);
            this.Load += new System.EventHandler(this.WaifuFrontend_Load);
            this.DragDrop += new System.Windows.Forms.DragEventHandler(this.Form1_DragDrop);
            this.DragEnter += new System.Windows.Forms.DragEventHandler(this.Form1_DragEnter);
            this.DetailsPanel.ResumeLayout(false);
            this.DetailsPanel.PerformLayout();
            this.ProcessorGroupBox.ResumeLayout(false);
            this.ProcessorGroupBox.PerformLayout();
            this.ModeGroupBox.ResumeLayout(false);
            this.ModeGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ScaleUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MainPictureBox)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel DetailsPanel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown ScaleUpDown;
        private System.Windows.Forms.RadioButton ReductionNoneButton;
        private System.Windows.Forms.RadioButton ReductionTwoButton;
        private System.Windows.Forms.RadioButton ReductionOneButton;
        private System.Windows.Forms.RadioButton ProcessCudnnButton;
        private System.Windows.Forms.RadioButton ProcessGpuButton;
        private System.Windows.Forms.RadioButton ProcessCpuButton;
        private System.Windows.Forms.GroupBox ModeGroupBox;
        private System.Windows.Forms.GroupBox ProcessorGroupBox;
        private System.Windows.Forms.Button DetailsButton;
        private System.Windows.Forms.PictureBox MainPictureBox;
        private System.Windows.Forms.Button RunButton;
        private System.Windows.Forms.RadioButton ReductionAutoButton;
        private System.Windows.Forms.TextBox OutputTextBox;


    }
}

